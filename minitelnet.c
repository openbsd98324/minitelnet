
// The OpenBSD and NetBSD Mini Telnet, To Pilot of Ports

//////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////// 
// 
// Free Software destined to remember few commands  
// BSD Licensing, Free.
// 
// Little Soft to take control of the machine, using a C compiler
// Edu Training about Socket Lvl. #2
//
//////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////// 


// LICENSING : 
/*-
 * Copyright (c) 2008 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// NetBSD adopted this 2-clause license in 2008 for code contributed to NetBSD Foundation. It is being deprecated as a separate license because it is a match to BSD-2-Clause. The line describing that the code is derived from software contributed to NetBSD is not viewed as being a substantive part of the license text.
// Reference : https://www.netbsd.org/about/redistribution.html


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>      
#include <arpa/inet.h>  
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
#include <dirent.h>   

#if defined(__linux__) //linux
#elif defined(__unix__) 
#define PATH_MAX 2046 
#endif
#include <time.h> 
    
#define TRUE   1 
#define FALSE  0 
#define PORT 8888 


char currentpath[2046];
int  mode_system = 0; 
int  mode_save_log = 0; 
int  skip_command = 0; 



///////////////////////////////////////////
///////////////////////////////////////////
void nls()
{ 
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( "." );
	while  ((dp = readdir( dirp )) != NULL ) 
	{
		if (  strcmp( dp->d_name, "." ) != 0 )
			if (  strcmp( dp->d_name, ".." ) != 0 )
				printf( "%s\n", dp->d_name );
	}
	closedir( dirp );
}





/////////////////////////////////
void proc_save_append_env(  char *myout  )
{
        FILE *fptt; 
        char cwd[PATH_MAX];
        char target[PATH_MAX];
        strncpy( target , getcwd( cwd, PATH_MAX ), PATH_MAX );
        strncat( target , "/" , PATH_MAX - strlen( target ) -1 );
        strncat( target , ".minitelnet.ini" , PATH_MAX - strlen( target ) -1 );
        fptt = fopen( target , "ab+" );
        fputs( myout , fptt );
        fclose( fptt );
}






void scr_clear()
{
}







/////////////////////////////////////////////////////////////
int main(int argc , char *argv[])  
{  
   printf( "*************************\n" );
   printf( " GNU Mini Telnet Service \n" );
   printf( "*************************\n" );
   printf( "\n" );
   printf( "Connect with: telnet 127.0.0.1 8888 \n" );
   printf( "or \n" );
   printf( "Connect with: telclient 127.0.0.1 8888 \n" );

  
   //char cwd[PATH_MAX];
   //cwd[0] = '\0';
   //strncpy( currentpath , getcwd( cwd, PATH_MAX ), PATH_MAX );

   scr_clear();

   int debugm = 0;
   FILE *fptt; 

   int j, i ;  
   int  chr = 0; 
   char str[PATH_MAX];
   int  fooi=0;
   char string[PATH_MAX];
   char linestr[PATH_MAX];


    int opt = TRUE;  
    int master_socket , addrlen , new_socket , client_socket[30] , max_clients = 30 , activity, valread , sd;  
    int max_sd;  
    struct sockaddr_in address;  

        
    //char buffer[1025];  //data buffer of 1K 
    char buffer[PATH_MAX];  //data buffer of 1K 
        
    //set of socket descriptors 
    fd_set readfds;  
        
    //a message 
    char *message = "ECHO Daemon v1.0 \r\n";  
    
    //initialise all client_socket[] to 0 so not checked 
    for (i = 0; i < max_clients; i++)  
    {  
        client_socket[i] = 0;  
    }  
        
    //create a master socket 
    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0)  
    {  
        perror("socket failed");  
        exit(EXIT_FAILURE);  
    }  
    
    //set master socket to allow multiple connections , 
    //this is just a good habit, it will work without this 
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, 
          sizeof(opt)) < 0 )  
    {  
        perror("setsockopt");  
        exit(EXIT_FAILURE);  
    }  
    
    //type of socket created 
    address.sin_family = AF_INET;  
    address.sin_addr.s_addr = INADDR_ANY;  
    address.sin_port = htons( PORT );  
        
    //bind the socket to localhost port 8888 
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0)  
    {  
        perror("bind failed");  
        exit(EXIT_FAILURE);  
    }  
    printf("Listener on port %d \n", PORT);  
        
    //try to specify maximum of 3 pending connections for the master socket 
    if (listen(master_socket, 3) < 0)  
    {  
        perror("listen");  
        exit(EXIT_FAILURE);  
    }  
        
    //accept the incoming connection 
    addrlen = sizeof(address);  
        

    while(TRUE)  
    {  
        //clear the socket set 
        FD_ZERO( &readfds );  
    
        //add master socket to set 
        FD_SET(master_socket, &readfds);  
        max_sd = master_socket;  
            
        //add child sockets to set 
        for ( i = 0 ; i < max_clients ; i++)  
        {  
            //socket descriptor 
            sd = client_socket[i];  
                
            //if valid socket descriptor then add to read list 
            if(sd > 0)  
                FD_SET( sd , &readfds);  
                
            //highest file descriptor number, need it for the select function 
            if(sd > max_sd)  
                max_sd = sd;  
        }  
    
        //wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely 
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
      
        //if ((activity < 0) && (errno!=EINTR))  
        //{  
            //printf("select error");  
        //}  
            
        //If something happened on the master socket , 
        //then its an incoming connection 
        if (FD_ISSET(master_socket, &readfds))  
        {  
            if ((new_socket = accept(master_socket, 
                    (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)  
            {  
                perror("accept");  
                exit(EXIT_FAILURE);  
            }  
            
            //send new connection greeting message 
            if( send(new_socket, message, strlen(message), 0) != strlen(message) )  
            {  
                perror("send");  
            }  
                
            puts("Welcome message sent successfully");  
                
            //add new socket to array of sockets 
            for (i = 0; i < max_clients; i++)  
            {  
                //if position is empty 
                if( client_socket[i] == 0 )  
                {  
                    client_socket[i] = new_socket;  
                    //printf("Adding to list of sockets as %d\n" , i);  
                    break;  
                }  
            }  
        }  
            
        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++)  
        {  
            sd = client_socket[i];  
            if (FD_ISSET( sd , &readfds))  
            {  
                //Check if it was for closing , and also read the 
                //incoming message 
                if ((valread = read( sd , buffer, 1024)) == 0)  
                {  
                    //Somebody disconnected , get his details and print 
                    getpeername(sd , (struct sockaddr*)&address , \
                        (socklen_t*)&addrlen);  
                    //printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
                        
                    //Close the socket and mark as 0 in list for reuse 
                    close( sd );  
                    client_socket[i] = 0;  
                }  
                    
                //Echo back the message that came in 
                else
                {  
                       //set the string terminating NULL byte on the end 
                       //of the data read 
		       printf( "== Stage 2.1\n" );
                       buffer[valread] = '\0';  
		       skip_command = 0; 

                       // cmd /new 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'n') 
                       if ( buffer[2] == 'e') 
                       if ( buffer[3] == 'w') 
                       {
                            scr_clear();
		            printf( "New...\n" );
		            skip_command = 1; 
                       }

                       // cmd /nls 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'n') 
                       if ( buffer[2] == 'l') 
                       if ( buffer[3] == 's') 
                       {
                            scr_clear();
		            printf( "New...\n" );
			    nls(); 
		            skip_command = 1; 
                       }

                       // cmd /log 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'l') 
                       if ( buffer[2] == 'o') 
                       if ( buffer[3] == 'g') 
                       {
                            scr_clear();
			    if ( mode_save_log == 0 ) mode_save_log = 1;  else mode_save_log = 0; 
		            printf( "New, Active/Unactive Save Log %d...\n", mode_save_log );
		            skip_command = 1; 
                       }

                       // cmd /system or /sys 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 's') 
                       if ( buffer[2] == 'y') 
                       if ( buffer[3] == 's') 
                       {
                            scr_clear();
			    if ( mode_system == 0 ) mode_system = 1;  else mode_system = 0; 
		            printf( "New, Active/Unactive System %d...\n", mode_system );
		            skip_command = 1; 
                       }


                       // cmd quit 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'q') 
                       if ( buffer[2] == 'u') 
                       if ( buffer[3] == 'i') 
                       if ( buffer[4] == 't') 
                       {
                           scr_clear();
		           printf( "Exit...\n" );
			   exit( 0 );
                       }

                       // cmd 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'e') 
                       if ( buffer[2] == 'x') 
                       if ( buffer[3] == 'i') 
                       if ( buffer[4] == 't') 
                       {
                           scr_clear();
		           printf( "Exit...\n" );
			   exit( 0 );
                       }


		       printf( "== Stage 2.2\n" );
                       strncpy(   string,  ":ECHO:\n"  , PATH_MAX );
                       send( sd , string , strlen( string ) , 0 );  
                       ////
                       strncpy(   string,  buffer , PATH_MAX );
                       send( sd , string , strlen( string ) , 0 );  
                       ////
                       strncpy( string, "\n" , PATH_MAX );
                       send( sd , string , strlen( string ) , 0 );  



                       // log
		       printf( "== Stage 2.3\n" );
                       snprintf( linestr , PATH_MAX , "%d/%d: Input %s", sd, max_clients , buffer );   
		       if ( mode_save_log == 1 )     proc_save_append_env( linestr );



		                 printf( "== Stage 2.4\n" );
				 char str[PATH_MAX];
				 char ptr[PATH_MAX];
                                 strncpy( str, buffer, PATH_MAX );
				 int i,j=0;
				 for(i=0; str[i]!='\0'; i++)
				 {
					 if ( str[i] != '\r' )
					  if ( str[i] != '\n') 
					   if ( str[i] != '\0') 
						 ptr[j++]=str[i];
				 } 
				 ptr[j]='\0';

				 strncpy( linestr, ptr, PATH_MAX );
                                 printf( "|Pass|(%s)|\n" ,  linestr );
                                 printf( "|Mode System|(%d)|\n" ,  mode_system );

                                 // skip command
		                 if ( skip_command == 0 )  
				 {
                                   if ( mode_system == 1 ) 
                                       system(  linestr );
				   else 
                                       printf( "Mode System Unactivated.\n" );
				 }

				 //strncpy( string, "\n" , PATH_MAX );
				 //send( sd , string , strlen( string ) , 0 );  
				 ///snprintf( string, sizeof(string),  "Command entered: |%s|\n" ,  linestr );
				 ///send( client_socket[ k ] , linestr , strlen( linestr ) , 0 );                         

				 //send( client_socket[ k ] , "LAST\n" , strlen( "LAST\n" ) , 0 );  


                         // new
                         int k ;  int userk = sd ;  char msgout[PATH_MAX];
                         for (k = 0; k < max_clients; k++)                                            
                         {                                                                            


                           if ( FD_ISSET( client_socket[ k ]  , &readfds))                                            
                           {  
                           } 
                           else 
                           {
                               snprintf( msgout ,  sizeof( msgout ), "%d Socket %d/%d/%d/%s: %s\n", (int)time(NULL), userk ,  client_socket[ k ] , max_clients,   " " ,  buffer );
                               send( client_socket[ k ] , msgout , strlen( msgout ) , 0 );                         
                               //set the string terminating NULL byte on the end                

                           }
                          }

                }  
            }  
        }  
    }  
        
    return 0;  
}









